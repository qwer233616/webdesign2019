from random import randint

def isNumFormatValid(strAns):
    if not strAns.isdigit():
        print('input is not digit')
        return False

    if len(strAns) != 4:
        print('number lenght is not 4')
        return False

    setNumber = set()

    for c in strAns:
        setNumber.add(c)

    if len(setNumber) < 4:
        print('duplicated number')
        return False

    return True

def calcHint(guess, ans):
    A = 0
    B = 0

    for i in range(0, 4):
        for j in range(0, 4):
            if guess[i] == ans[j]:
                if i == j:
                    A += 1
                else:
                    B += 1

    return A, B

def Game():
    print('start guessing!!')

    # 隨機產生答案
    isValid = False

    # 如果格式不符，就繼續生答案
    while not isValid:
        ans = randint(0, 9999)
        strAns = '{:04d}'.format(ans)
        isValid = isNumFormatValid(strAns)    

    print('ans', strAns)

    isValid = False

    while not isValid:
        guess = input('請輸入答案:')
        isValid = isNumFormatValid(guess)

    print('guess', guess)

    A, B = calcHint(guess, strAns)
    print('{}A{}B'.format(A, B))

    while A < 4:
        guess = input('請輸入答案:')
        isValid = isNumFormatValid(guess)

        while not isValid:
            guess = input('數字格式錯誤，請再輸入一次:')
            isValid = isNumFormatValid(guess)

        A, B = calcHint(guess, strAns)
        print('{}A{}B'.format(A, B))

    print('Game over')

Game()