from random import randint

pool = []

def hintAnalysis(hint):
    if hint[1] != 'A' or hint[3] != 'B':
        return False, 0, 0

    if not hint[0].isdigit() or not hint[2].isdigit():
        return False, 0, 0

    A = int(hint[0])
    B = int(hint[2])

    if A + B > 4:
        return False, 0, 0

    return True, A, B

def isNumberValid(num):
    if not num.isdigit():
        return False

    if len(num) != 4:
        return False

    s = set()

    for n in num:
        s.add(n)

    if len(s) != 4:
        return False
    
    return True

def refinePool(pool, num, hintA, hintB):
    newPool = []

    for p in pool:
        A = 0
        B = 0

        for i in range(0, 4):
            for j in range(0, 4):
                if p[i] == num[j]:
                    if i == j:
                        A += 1
                    else:
                        B += 1

        if A == hintA and B == hintB:
            newPool.append(p)

    return newPool  

def game():
    pool = []
    
    for i in range(1, 9999):
        num = '{:04d}'.format(i)

        if isNumberValid(num):
            pool.append(num)

    A = 0

    while A < 4:
        print(pool)
        idx = randint(0, len(pool) - 1)
        comGuess = pool[idx]
        print('電腦猜 {}'.format(comGuess))

        while True:
            hint = input('玩家提示:')
            rst, A, B = hintAnalysis(hint)

            if rst:
                break

        if A == 4:
            print('player win')
        else:
            pool= refinePool(pool, comGuess, A, B)

        if len(pool) < 1:
            print('error')
            break

def main():
    game()

if __name__ == '__main__':
    main()