function AjaxPost(url, postData, callback, callbackErr) {
    $.ajax({
      type: "POST",
      url: url,
      data: postData,
      success: function (data) {
        if (callback && typeof (callback) == "function")
          callback(data);
      },
      error: function (err) {
        if (callbackErr && typeof (callbackErr) == "function")
          callbackErr(err);
      }
    });
}
