from django.urls import include, path

import api.service as service

urlpatterns = [
    path('register', service.register),
]